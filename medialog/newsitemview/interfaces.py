from zope.interface import Interface
    
class INewsitemObject(Interface):
    """Marker interface for marking an object"""
    
class IFolderObject(Interface):
    """Marker interface for marking a folder"""